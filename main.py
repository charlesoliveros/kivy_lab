from kivy import require
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.config import Config

require('2.0.0')
Config.set('graphics', 'width', 400)
Config.set('graphics', 'height', 800)


class BoxMain(BoxLayout):
    '''La apariencia de los widgets (como este box), se modifican
    en el archivo main.kv
    '''
    pass


class MainApp(App):
    '''Esta clase debe iniciar con el nombre Main (igual al main.kv)
    '''

    title = 'Hola Mundo'

    def build(self):
        return BoxMain()


if __name__ == '__main__':
    MainApp().run()
